A transcription of Paul Otlet's [*Traité de Documentation: le livre sur le livre, théorie et pratique*](https://en.wikipedia.org/wiki/Trait%C3%A9_de_Documentation)

## Table of contents

- [About this transcription](#about-this-transcription)
    - [Why PDF?](#why-pdf)
- [Transcribing the Traité](#transcribing-the-traite)
- [About Paul Otlet](#about-paul-otlet)
- [About the Traité](#about-the-traite)
- [Crowd sourcing for proofreading and translation](#crowd-sourcing-for-proofreading-and-translation)
	- [Translation](#translation)
- [Guidelines](#guidelines)
	- [Page layout](#page-layout)
	- [Special characters](#special-characters)

# About this transcription

This repo is a fork of a [transcription in reStructuredText](https://github.com/DavidMercier/traite)
which is itself a fork of the original transcription initiated at <https://github.com/PaulOtlet/traite>.
Indeed, some precious corrections were added.
The PDF produced by [another fork](https://github.com/textvs/Otlet1934)
-- a transcription in the Markdown markup language; has also been used as a reference,
mainly for the structure because it does not benefit from the corrections added by [David Mercier](https://github.com/DavidMercier).

This transcription differs by the format employed for the markup of the source file,
which is Org.
This powerful and volatile markup language was created for an [Emacs mode](http://orgmode.org/).
It allows authors, among other features,
to produce clean documents in many output formats such as PDF (via [LaTeX](https://en.wikipedia.org/wiki/LaTeX))
while keeping a light and consistent syntax,
which is consequently [easy and natural to learn](http://karl-voit.at/2017/09/23/orgmode-as-markup-only/).

The ease of the LateX integration permits control over the produced document
without the heaviness of a document fully typeset in LaTeX.
When editting in the Emacs editor,
disable line numbers and expand only one part at a time as the file is quite large
(it may be useful to split it).

## Why PDF ?

This transcription doesn't target EPUB as it is not easily readable on most desktop systems, and more generally [doesn't guarantee the quality of the displayed document](https://practicaltypography.com/why-theres-no-e-book-or-pdf.html#the-problem-with-e-books). While not ideal, the PDF format is sufficiently common and LaTex ensures a good enough level of quality.

A web format could also be considered, but the book was written at a time at which pictures in books couldn't be animated or for example embed audio (*sad times!*). That's why a web version is not considered a priority, unless the original book is enriched with multimedia content.

# Transcribing the Traité

Help to transcribe Otlet's "Traité" from PDF to a digital full-text edition. No easily readable edition of his works is currently accessible online. While more and more books and articles are published about the "visionary of the digital age", the original sources remain accessible only to those who are willing to download or browse a 200 MB PDF-file and understand French.

The first goal is to produce a digital edition of the "Traité de Documentation" in a clean PDF format.

To contribute to this effort just get a copy of the [PDF of University of Ghent](http://lib.ugent.be/fulltxt/RUG01/000/990/276/BIB-038A006_2006_0001_AC.pdf), open a text editor and start correcting the mistakes of the OCR run according to the rules layed out below.

# About Paul Otlet

<http://en.wikipedia.org/wiki/Paul_Otlet>

# About the Traité

- [Wikipedia.org: Traité de Documentation](https://en.wikipedia.org/wiki/Trait%C3%A9_de_Documentation)
- [Archive.org](https://archive.org/details/OtletTraitDocumentationUgent)
- [OpenLibrary.org ](https://openlibrary.org/works/OL1112871W/Traite%CC%81_de_documentation)
- Gent University
	- <http://search.ugent.be/meercat/x/view/rug01/000990276>
	- <http://lib.ugent.be/fulltxt/handle/1854/5612/>
	- <http://lib.ugent.be/catalog/rug01:000990276>
	- http://buck.ugent.be/fulltxt/RUG01/000/990/276/BIB-038A006_2006_0001_AC.pdf>
- <https://www.google.co.in/search?tbo=p&tbm=bks&q=inauthor:%22Paul+Otlet%22>

<https://archive.org/stream/OtletTraitDocumentationUgent/OtletTraitDocumentationUgent_djvu.txt> compared against FineReader 12 on 2014-04-05, FineReader was remarkably
better. Seems that the text version on Archive.org was updated in the
meantime.

Link to / show individual pages:
<https://archive.org/stream/OtletTraitDocumentationUgent#page/n24/mode/1up>
is page 22. but 179/180 are doubled

# Crowd sourcing for proofreading and translation

- <http://wikisource.org>
- <http://traduwiki.org/>
- <https://www.transifex.com/>
- <http://crowdin.net/pricing>
- <https://poeditor.com/>
- <http://www.hasecke.eu/Members/juh/sphinx-a-tool-for-self-publisher>
- <https://www.discovermeteor.com/blog/community-translations-with-github-middleman-codeship-heroku/>
- <http://annotatedbooksonline.com>

## Translation

- <https://translate.google.com/>
- <http://translation.babylon.com/french/to-english/>

# Guidelines

## Page layout

Pages should be structured like this:

    ====

    *<pg-nr>*　CHAPTER TITLE　<section-nr>  (can be mirrored, depending on even/odd page)

    ... texttext texttext texttext texttext texttext
    Before and after the chapter and section titles, 
    insert a unicode "wide space", copy-paste from "　", or U+3000 

    Line breaks should be placed as in the
    original text, including hyphenation. 
    For more details on hyphenation, see below...

    Paragraphs are separated by a blank line.
    ... texttext texttext texttext texttext texttext ...
    After the first column (and optional footnotes) put at least one
    blank line and then a horizontal bar ("----"):

    ----

    next column texttext texttext texttext texttext texttext texttext
    texttext texttext texttext texttext texttext texttext texttext texttext
    text texttext texttext texttext texttext texttext


    ====

    <section-nr>　CHAPTER TITLE *<pg-nr>*

    next page text ...
    text ...

In detail:

- Pages are separated by horizontal rules "====" surrounded by two empty lines above and one blank line below the rule.
- After a single blank line, page numbers, section/chapter titles and section numbers should be placed just as in the PDF, pages in *italics* (surrounded by `*` asterisks.)
- The two columns on each page are separated by "----" surrounded by two single blank lines.
- Header markup should be one symbol longer than the header text
- Unicode characters should be used wherever possible, but should be documented somewhere
- The original text, layout and typesetting should be represented as close as possible within the limits of the reStructuredText format.
- Hyphenation: What is a good solution to somehow mark a hyphenation from the book in the reST format without it showing up in the compiled html? Leaving hyphenation in the reST would destroy full-text search. But single dashes (`-`) at the end of the line are easy to remove with a script. Soft-hyphens can be used to replace the hyphenation so that it doesn't show up in the browser. An open question is how this will affect the epub output of Sphinx.
- If the PDF resolution is not good enough to discriminate symbols accurately, just place three bold question marks **???** (surround them with two asterisks `**???**`) in this position.

## Special characters

see also: <http://character-code.com/french-html-codes.php>

« .. » left and right angle quotes

― Horizontal rule (use above footnotes)

– EN Dash

— EM Dash

see also: <http://en.wikipedia.org/wiki/Dash>

The source files are encoded in unicode format (UTF-8 without BOM). To
check the encoding on Windows, use the excellent
<http://notepad-plus-plus.org> editor.

To input specific letters or symbols:

Ubuntu12: Shift+AltGr + \^ + e = ê

<https://de.wikipedia.org/wiki/%C5%92> U+0152 OEvre Œ, Kleinbuchstabe œ
U+0153

Multiplication sign: × (Unicode 215)

<https://help.ubuntu.com/community/ComposeKey> Unicode composition:
Another means to enter characters is to enter them as Unicode character
number.

Press Shift+Ctrl+U, release U, enter the hexadecimal (0123456789abcdef)
Unicode character code point, then release Shift+Ctrl. An underlined u
followed by the number will be displayed as you type.

Alternatively, press (and release) Shift+Ctrl+U, then, while underlined
u is displayed, enter the hexadecimal Unicode character code point
followed by &lt;Return&gt;.

<http://en.wikipedia.org/wiki/Unicode_input>

Windows:
<http://superuser.com/questions/47420/insert-unicode-characters-via-the-keyboard>

